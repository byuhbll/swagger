package edu.byu.hbll.swagger;

import static junit.framework.TestCase.assertEquals;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.glassfish.jersey.test.DeploymentContext;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.ServletDeploymentContext;
import org.glassfish.jersey.test.grizzly.GrizzlyWebTestContainerFactory;
import org.glassfish.jersey.test.spi.TestContainerFactory;
import org.junit.Test;

/** Created by bwelker on 11/21/16. */
public class CorsProviderIT extends JerseyTest {

  @Override
  protected Application configure() {
    return new ResourceConfig().packages("edu.byu.hbll.swagger");
  }

  @Override
  protected TestContainerFactory getTestContainerFactory() {
    return new GrizzlyWebTestContainerFactory();
  }

  @Override
  protected DeploymentContext configureDeployment() {
    ResourceConfig config = new ResourceConfig().packages("edu.byu.hbll.swagger");
    return ServletDeploymentContext.forServlet(new ServletContainer(config)).build();
  }

  @Test
  public void testCorsHeadersNotAddedToResponse() {
    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
    headers.add("origin", "http://petstore.swagger.io");
    Response response =
        target("swagger")
            .request()
            .property(ClientProperties.FOLLOW_REDIRECTS, false)
            .headers(headers)
            .get();
    assertEquals(null, response.getHeaderString("Access-Control-Allow-Origin"));
    assertEquals(null, response.getHeaderString("Access-Control-Allow-Methods"));
    assertEquals(null, response.getHeaderString("Access-Control-Allow-Headers"));
  }
}
