package edu.byu.hbll.swagger;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.glassfish.jersey.test.DeploymentContext;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.ServletDeploymentContext;
import org.glassfish.jersey.test.grizzly.GrizzlyWebTestContainerFactory;
import org.glassfish.jersey.test.spi.TestContainerFactory;
import org.junit.Test;

/** Created by bwelker on 11/21/16. */
public class SwaggerApiIT extends JerseyTest {

  @Override
  protected Application configure() {
    return new ResourceConfig().packages("edu.byu.hbll.swagger");
  }

  @Override
  protected TestContainerFactory getTestContainerFactory() {
    return new GrizzlyWebTestContainerFactory();
  }

  @Override
  protected DeploymentContext configureDeployment() {
    ResourceConfig config = new ResourceConfig().packages("edu.byu.hbll.swagger");
    return ServletDeploymentContext.forServlet(new ServletContainer(config)).build();
  }

  @Test
  public void testRedirect() {
    Response response =
        target("swagger").request().property(ClientProperties.FOLLOW_REDIRECTS, false).get();
    assertEquals(307, response.getStatus());
    assertEquals(
        "http://petstore.swagger.io/?url=http%3A%2F%2Flocalhost%3A9998%2Fswagger%2Fyaml",
        response.getLocation().toString());
  }

  @Test
  public void testForwardedRedirect() {
    Response response =
        target("swagger")
            .request()
            .property(ClientProperties.FOLLOW_REDIRECTS, false)
            .header("x-forwarded-proto", "https")
            .header("x-forwarded-port", "443")
            .get();
    assertEquals(307, response.getStatus());
    assertEquals(
        "http://petstore.swagger.io/?url=https%3A%2F%2Flocalhost%3A443%2Fswagger%2Fyaml",
        response.getLocation().toString());
  }

  @Test
  public void testYamlEndpoint() {
    Response response = target("swagger/yaml").request().get();
    assertEquals(200, response.getStatus());
    assertEquals("#This is a yaml file", response.readEntity(String.class));
  }
}
