package edu.byu.hbll.swagger;

import java.io.InputStream;
import java.net.URI;
import java.util.Scanner;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

/** Created by Ben Welker on 11/17/16. */
@Path("swagger")
public class SwaggerApi {

  /** The url where the Swagger UI is hosted that the user will be redirected to. */
  public static final String SWAGGER_URL = "http://petstore.swagger.io?url={url}";

  @Context private HttpServletRequest request;

  @Context private UriInfo uriInfo;

  /**
   * Redirects the user to the Swagger UI with the appropriate yaml file in the url.
   *
   * @return 307 Redirect
   */
  @GET
  public Response goToSwagger() {
    String url = getYamlUri();
    return Response.temporaryRedirect(UriBuilder.fromUri(SWAGGER_URL).build(url)).build();
  }

  /**
   * Builds a Yaml uri (ex: https://example.com:8443/contextroot/swagger/yaml). Uses the scheme,
   * host, and port provided in the x-forwarded-* headers if present. Otherwise, uses values
   * provided in the context UriInfo.
   *
   * @return Yaml uri.
   */
  private String getYamlUri() {

    UriBuilder builder = uriInfo.getBaseUriBuilder();

    // Replace the scheme if the forward header is present.
    if (request.getHeader("x-forwarded-proto") != null) {
      builder.scheme(request.getHeader("x-forwarded-proto"));
    }

    // Replace the port if the forward header is present.
    if (request.getHeader("x-forwarded-port") != null) {
      builder.port(Integer.parseInt(request.getHeader("x-forwarded-port")));
    }

    // Replace the host if the forward header is present. This is just in case, but it shouldn't
    // make any difference.
    if (request.getHeader("x-forwarded-host") != null) {
      builder.host(request.getHeader("x-forwarded-host"));
    }

    // Add the correct path to the swagger definition.
    builder.path("/swagger/yaml");

    URI uri = builder.build();
    return uri.toString();
  }

  /**
   * Returns swagger.yml to the client. In order for this to work, swagger.yml must be placed in the
   * resources folder.
   *
   * @return resources/swagger.yml
   */
  @Path("yaml")
  @GET
  @Produces("application/x-yaml")
  public Response getSwaggerYaml() {

    try (InputStream is = getClass().getResourceAsStream("/swagger.yml");
        Scanner in = new Scanner(is)) {
      in.useDelimiter("\\Z");
      String content = in.next();

      return Response.ok().entity(content).build();
    } catch (Exception e) {
      return Response.serverError().build();
    }
  }
}
