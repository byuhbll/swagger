package edu.byu.hbll.swagger;

import java.io.IOException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

/**
 * Adds the CORS (cross-origin resource sharing) header, needed for the Swagger UI to function with
 * the apis. Created by Ben Welker on 11/17/16.
 */
@Provider
public class CorsProvider implements ContainerResponseFilter {

  /**
   * Adds the required headers for Swagger UI to function correctly. These headers are added to
   * every response produced by an application using this.
   *
   * @param req Request Context
   * @param res Response Context
   * @throws IOException Never
   */
  @Override
  public void filter(ContainerRequestContext req, ContainerResponseContext res) throws IOException {
    if ("http://petstore.swagger.io".equals(req.getHeaderString("origin"))) {
      res.getHeaders().putSingle("Access-Control-Allow-Origin", "http://petstore.swagger.io");
      res.getHeaders()
          .putSingle("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, PATCH, OPTIONS");
      res.getHeaders()
          .putSingle("Access-Control-Allow-Headers", "Content-Type, api_key, Authorization");
    }
  }
}
