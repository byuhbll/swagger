[![Build Status](https://semaphoreci.com/api/v1/byuhbll/swagger/branches/dev/shields_badge.svg)](https://semaphoreci.com/byuhbll/swagger)

#Swagger

Swagger is a framework for documenting the APIs available in an application. This library facilitaties using swagger in a JavaEE application.

To include Swagger in your application, include this library in your pom.xml. Place your swagger definition named swagger.yml in the resources folder,
alongside the logback.xml if your application includes one. This library will create a new endpoint in your application ("/swagger") that will redirect
the user to the hosted swagger docs and load previously mentioned swagger.yml.